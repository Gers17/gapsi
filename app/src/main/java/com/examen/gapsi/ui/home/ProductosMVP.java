package com.examen.gapsi.ui.home;

import com.examen.gapsi.domain.entity.Products;
import com.examen.gapsi.ui.base.BasePresenter;
import com.examen.gapsi.ui.base.BaseView;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public interface ProductosMVP {

    interface Model{
        Observable<List<Products>> getMatches(String productName);
    }

    interface Presenter extends BasePresenter<View> {
        void loadProducts(String productName);
    }

    interface View extends BaseView {
        void updateUI(List<Products> products);
        void withoutItems();
    }
}
