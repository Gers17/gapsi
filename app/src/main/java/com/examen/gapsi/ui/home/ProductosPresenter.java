package com.examen.gapsi.ui.home;

import android.annotation.SuppressLint;

import com.examen.gapsi.ui.base.BaseView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ProductosPresenter implements ProductosMVP.Presenter {

    private ProductosMVP.Model model;
    private ProductosMVP.View view;


    public ProductosPresenter(ProductosMVP.Model model) {
        this.model = model;
    }

    @SuppressLint("CheckResult")
    @Override
    public void loadProducts(String productName) {
        model.getMatches(productName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( products -> {
                    if (view!= null){
                        if (products.size() > 0){
                            view.updateUI(products);
                        } else {
                            view.withoutItems();
                        }
                    }
                },
                        error -> {
                            if (view!=null){
                                view.withoutItems();
                            }
                        });

    }


    @Override
    public void onAttach(ProductosMVP.View mvpView) {
    view = mvpView;
    }

    @Override
    public void onDetach() {
    view = null;
    }
}
