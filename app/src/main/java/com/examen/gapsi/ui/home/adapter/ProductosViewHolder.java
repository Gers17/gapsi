package com.examen.gapsi.ui.home.adapter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.examen.gapsi.databinding.ItemProductBinding;
import com.examen.gapsi.domain.entity.Products;

public class ProductosViewHolder extends RecyclerView.ViewHolder {

    ItemProductBinding binding;

    public ProductosViewHolder(ItemProductBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void addProduct(Products products){
        binding.tvPriceProduct.setText("$ " + products.getPrice());
        binding.tvProduct.setText(products.getName());

        Glide.with(itemView.getContext())
                .load(products.getImage())
                .into(binding.ivProduct);
    }


}
