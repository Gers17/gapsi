package com.examen.gapsi.ui.home.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.examen.gapsi.databinding.ItemProductBinding;
import com.examen.gapsi.domain.entity.Products;

import java.util.List;

public class ProductosAdapter extends RecyclerView.Adapter<ProductosViewHolder> {

    private List<Products> productsList;

    public ProductosAdapter(List<Products> productsList) {
        this.productsList = productsList;
    }

    @NonNull
    @Override
    public ProductosViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductosViewHolder(ItemProductBinding.inflate(LayoutInflater.from(parent.getContext()),parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ProductosViewHolder holder, int position) {
        holder.addProduct(productsList.get(position));

    }

    @Override
    public int getItemCount() {
        return productsList != null ? productsList.size() : 0;
    }
}
