package com.examen.gapsi.ui.home;

import com.examen.gapsi.data.DataManager;
import com.examen.gapsi.domain.entity.Products;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public class ProductosModel implements ProductosMVP.Model{

    DataManager dataManager;

    public ProductosModel(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public Observable<List<Products>> getMatches(String productName) {
        return dataManager.getProducts(productName);
    }
}
