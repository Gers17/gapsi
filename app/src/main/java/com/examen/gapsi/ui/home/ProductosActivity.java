package com.examen.gapsi.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.examen.gapsi.databinding.ActivityMainBinding;
import com.examen.gapsi.domain.entity.Products;
import com.examen.gapsi.ui.base.BaseActivity;
import com.examen.gapsi.ui.home.adapter.ProductosAdapter;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import javax.inject.Inject;

public class ProductosActivity extends BaseActivity implements ProductosMVP.View {

    ActivityMainBinding binding;
    @Inject ProductosMVP.Presenter presenter;
    private String productName;
    private ProductosAdapter productosAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.loadAnim.setVisibility(View.GONE);


        setUp();
    }

    @Override
    protected void setUp() {
    getComponent().inject(this);
    presenter.onAttach(this);
    binding.btSearch.setOnClickListener( v -> {
        productName = binding.etSearch.getText().toString().trim();
        if (!productName.equals("")){
            binding.loadAnim.setVisibility(View.VISIBLE);
            binding.rvProductos.setVisibility(View.GONE);
            presenter.loadProducts(productName);
            binding.etSearch.setText("");
        }else {
            Snackbar.make(binding.getRoot(),"Por favor escribe una palabra",Snackbar.LENGTH_SHORT).show();
        }
    });
    }

    @Override
    public void updateUI(List<Products> products) {
        binding.loadAnim.setVisibility(View.GONE);
        binding.rvProductos.setVisibility(View.VISIBLE);

        productosAdapter = new ProductosAdapter(products);
        binding.rvProductos.setLayoutManager(new LinearLayoutManager(this));
        binding.rvProductos.setAdapter(productosAdapter);
        productosAdapter.notifyDataSetChanged();

    }

    @Override
    public void withoutItems() {
        Toast.makeText(this,"Esta busqueda no tiene resultados disponibles",Toast.LENGTH_SHORT).show();

    }
}