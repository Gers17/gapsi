package com.examen.gapsi.ui.base;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.examen.gapsi.di.component.ActivityComponent;
import com.examen.gapsi.di.component.DaggerActivityComponent;
import com.examen.gapsi.di.modules.ActivityModule;
import com.examen.gapsi.di.modules.UtilsModule;

public abstract class BaseActivity extends AppCompatActivity {

    private ActivityComponent component;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    protected abstract void setUp();

    protected ActivityComponent getComponent(){
        if(component==null){
            component = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(this))
                    .utilsModule(new UtilsModule(this))
                    .build();
        }
        return component;
    }


}
