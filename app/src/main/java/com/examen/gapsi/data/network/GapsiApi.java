package com.examen.gapsi.data.network;

import com.examen.gapsi.domain.entity.Products;
import com.examen.gapsi.domain.entity.ResponseGeneric;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface GapsiApi {

    @GET("demo-gapsi/search")
    @Headers("X-IBM-Client-Id: adb8204d-d574-4394-8c1a-53226a40876e")
    Observable<ResponseGeneric> getResults(@Query("query") String product);
}
