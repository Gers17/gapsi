package com.examen.gapsi.data;

import com.examen.gapsi.domain.entity.Products;

import java.util.List;

import io.reactivex.Observable;

public interface DataManager {

    Observable<List<Products>> getProducts(String productName);
}
