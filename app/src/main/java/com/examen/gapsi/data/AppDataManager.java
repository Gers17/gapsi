package com.examen.gapsi.data;

import com.examen.gapsi.data.network.GapsiApi;
import com.examen.gapsi.domain.entity.Products;
import com.examen.gapsi.domain.entity.ResponseGeneric;
import com.google.gson.Gson;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

public class AppDataManager implements DataManager {
    GapsiApi gapsiApi;

    public AppDataManager(GapsiApi gapsiApi) {
        this.gapsiApi = gapsiApi;
    }

    @Override
    public Observable<List<Products>> getProducts(String productName) {

        return gapsiApi.getResults(productName)
                .flatMap((Function<ResponseGeneric, ObservableSource<List<Products>>>) genericResponse -> {

                    return Observable.just(genericResponse.getProductsList());
                });

    }
}
