package com.examen.gapsi.domain.entity;

import com.google.gson.annotations.SerializedName;

import retrofit2.http.GET;

public class Products {
    @SerializedName("image")
    private String image;

    @SerializedName("price")
    private float price;

    @SerializedName("title")
    private String name;

    @SerializedName("rating")
    private double rating;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
