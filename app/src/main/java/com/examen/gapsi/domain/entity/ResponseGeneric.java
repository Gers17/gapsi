package com.examen.gapsi.domain.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGeneric {
    private int totalResults;
    private int page;
    @SerializedName("items")
    private List<Products> productsList;

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<Products> getProductsList() {
        return productsList;
    }

    public void setProductsList(List<Products> productsList) {
        this.productsList = productsList;
    }
}
