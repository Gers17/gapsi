package com.examen.gapsi.di.modules;

import android.content.Context;


import com.examen.gapsi.data.DataManager;
import com.examen.gapsi.ui.home.ProductosMVP;
import com.examen.gapsi.ui.home.ProductosModel;
import com.examen.gapsi.ui.home.ProductosPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    Context context;

    public ActivityModule(Context context) {
        this.context = context;
    }

    @Provides
    public Context provideContext(){
        return context;
    }

    @Provides
    public ProductosMVP.Model proviodesProductsModel(DataManager dataManager){
        return new ProductosModel(dataManager);
    }

    @Provides
    public ProductosMVP.Presenter providesProductsPresenter(ProductosMVP.Model model){
        return new ProductosPresenter(model);
    }


}
