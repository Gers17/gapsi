package com.examen.gapsi.di.component;

import com.examen.gapsi.data.AppDataManager;
import com.examen.gapsi.di.modules.APIModule;

import dagger.Component;

@Component (modules = {APIModule.class})
public interface DataManagerComponent {
    void inject(AppDataManager appDataManager);
}
