package com.examen.gapsi.di.modules;

import android.content.Context;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.examen.gapsi.data.AppDataManager;
import com.examen.gapsi.data.DataManager;
import com.examen.gapsi.data.network.GapsiApi;

import dagger.Module;
import dagger.Provides;

@Module
public class UtilsModule {

    Context context;

    public UtilsModule(Context context) {
        this.context = context;
    }

    @Provides
    public LinearLayoutManager provideLinearLayoutManager(){
        return new LinearLayoutManager(context);
    }

    @Provides
    public GridLayoutManager provideGridLayoutManager(){
        return new GridLayoutManager(context,1);
    }

    @Provides
    public DataManager provideDataManager(GapsiApi gapsiApi){
        return new AppDataManager(gapsiApi);
    }





}
