package com.examen.gapsi.di.component;

import com.examen.gapsi.di.modules.APIModule;
import com.examen.gapsi.di.modules.ActivityModule;
import com.examen.gapsi.di.modules.UtilsModule;
import com.examen.gapsi.ui.home.ProductosActivity;

import dagger.Component;

@Component (modules = {ActivityModule.class, UtilsModule.class, APIModule.class})
public interface ActivityComponent {

    void inject(ProductosActivity productosActivity);

}
