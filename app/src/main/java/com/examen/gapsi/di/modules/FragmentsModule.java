package com.examen.gapsi.di.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class FragmentsModule {

    private Context context;

    public FragmentsModule(Context context) {
        this.context = context;
    }

    @Provides
    public Context provideContext(){
        return context;
    }


}
